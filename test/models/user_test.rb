require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = User.new(name: 'Test',email: 'test@mail.ru',password: "foobar", password_confirmation: "foobar")
  end
  test "should be valid" do
    assert @user.valid?
  end
  test "name should be present" do
    @user.name = "      "
    assert_not @user.valid?
  end

  test "email should be present" do
    @user.email = "    "
    assert_not @user.valid?
  end


  test "email should not be too long" do
    @user.email = "a" * 244 + "@example.com"
    assert_not @user.valid?
  end

  test "email should be accept valid addresses" do
    valid_email = %w[adilet571@gmail.com ad@mail.ru ad_931@mail.ru]

    valid_email.each do |email|
      @user.email = email
      assert @user.valid?, "#{email.inspect} should be valid"
    end
  end
    test "email should not be accept valid addresses" do
      invalid_email = %w[ad.mail.ru adilet.gmail.com]
      invalid_email.each do |email|
        @user.email = email
        assert_not @user.valid?, "#{email.inspect} should not be valid"
      end
    end


end
